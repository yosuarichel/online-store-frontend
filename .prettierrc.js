module.exports = {
    bracketSpacing: true,
    jsxBracketSameLine: true,
    singleQuote: true,
    trailingComma: 'all',
    eslintIntegration: true,
    stylelintIntegration: true,
    tabWidth: 4,
    endOfLine: auto,
    // Override any other rules you want
    arrowParens: 'always',
    printWidth: 100,
    proseWrap: 'always',
    semi: true,
    useTabs: false,
};