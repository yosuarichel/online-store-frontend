const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
    mode: 'development',
    devServer: {
        historyApiFallback: true,
        compress: true,
        hot: true,
        port: 8000,
        host:'127.0.0.1',
        // https: true
        // filename: 'bundle.js'
        disableHostCheck: true
    },
    devtool: 'eval',
    watch: true,
    performance: {
        hints: 'warning',
    }
});