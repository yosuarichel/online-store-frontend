module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    commonjs: true,
    jest: true,
  },
  settings: {
    react: {
      version: 'detect',
    }
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'airbnb',
    'plugin:jsx-a11y/recommended',
    'prettier',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'babel',
    'import',
    'jsx-a11y',
    'prettier',
  ],
  rules: {
    // 'prettier/prettier': [
    //   'error',
    //   {
    //     singleQuote: true,
    //     trailingComma: 'all',
    //   }
    // ],
    'react/jsx-filename-extension': 'off',
    'import/no-unresolved': ['error', {'commonjs': true, 'caseSensitive': true}],
    'indent' : ['error', 4, {'ignoredNodes': ['JSXElement', 'JSXElement > *', 'JSXAttribute', 'JSXIdentifier', 'JSXNamespacedName', 'JSXMemberExpression', 'JSXSpreadAttribute', 'JSXExpressionContainer', 'JSXOpeningElement', 'JSXClosingElement', 'JSXText', 'JSXEmptyExpression', 'JSXSpreadChild']}],
    'linebreak-style': 0,
    'quotes': [ 'error', 'single' ],
    'semi': [ 'error', 'always'],
    'no-console': ['warn'],
    'react/jsx-indent': ['error', 4],
    'react/jsx-boolean-value': 0,
    'react/jsx-closing-bracket-location': 1,
    'react/jsx-curly-spacing': [2, 'always'],
    'react/jsx-indent-props': [1, 2],
    'react/jsx-no-undef': 1,
    'react/jsx-uses-react': 1,
    'react/jsx-uses-vars': 1,
    'react/jsx-wrap-multilines': 1,
    'react/react-in-jsx-scope': 1,
    'react/prefer-es6-class': 1,
    'react/jsx-no-bind': 1,
  },
  ignorePatterns: [ "build/**" ] 
};
