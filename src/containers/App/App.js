import React from 'react';
import { Container } from 'react-bootstrap';
// import styled from 'styled-components';

import './App.scss';

import Navigation from '../../components/NavBar/NavBar';
import Routing from '../../routes/NavRouting/NavRouting';
import '../../components/Fontawesome/Fontawesome';

// const  Wrapper = styled.section`
//     background: #521818;
//     height: 900px;
// `;

const App = () => {
    return (
        <>
            <Navigation />
            <Container fluid>
                <Routing />
            </Container>
            
        </>
    );    
};

export default App;
