import React from 'react';
import { Row } from 'react-bootstrap';
import { LazyLoadComponent } from 'react-lazy-load-image-component';

// Component
import Carousel from '../../../components/Carousel/Carousel';

// Styles
import './Portal.scss';


const Home = () => (
    <div>
        <Row className="Row">
            <LazyLoadComponent>
                <Carousel />
            </LazyLoadComponent>
        </Row>
        <Row className="Row">
            test
        </Row>
    </div>
);

export default Home;
