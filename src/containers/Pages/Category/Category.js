import React from 'react';

// Component
import Section1 from '../../../components/Sections/Category/Section1/Section1';
// import Section2 from '../../../components/Sections/Category/Section2/Section2';
import Section3 from '../../../components/Sections/Category/Section3/Section3';
// import Section4 from '../../../components/Sections/Category/Section3/Section3';
import Section5 from '../../../components/Sections/Category/Section5/Section5';

// Styles
import './Category.scss';

const Category = () => (
    <>
        <Section1 />
        {/* <Section2 /> */}
        <Section3 />
        {/* <Section4 /> */}
        <Section5 />
    </>
);

export default Category;
