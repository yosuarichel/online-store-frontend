import React from 'react';
// import styled from 'styled-components';

// Component
import Section1 from '../../../components/Sections/Home/Section1/Section1';
// import Section2 from '../../../components/Sections/Home/Section2/Section2';
import Section3 from '../../../components/Sections/Home/Section3/Section3';
import Section4 from '../../../components/Sections/Home/Section4/Section4';
import Section5 from '../../../components/Sections/Home/Section5/Section5';
import Section6 from '../../../components/Sections/Home/Section6/Section6';
import Section7 from '../../../components/Sections/Home/Section7/Section7';
import Section8 from '../../../components/Sections/Home/Section8/Section8';

// Styles
import './Home.scss';

// const  Wrapper = styled.div`
//     background: #f4f6ff;
// `;


const Home = () => (
    <>
        <Section1 />
        {/* <Section2 /> */}
        <Section3 />
        <Section4 />
        <Section5 />
        <Section6 />
        <Section7 />
        <Section8 />
    </>
);

// Home.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };

export default Home;
