import React from 'react';
import propTypes from 'prop-types';
import { Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'; // import the react-router-dom components

const NavItem = ({ path, navTitle }) => (
    <Nav.Link as={ NavLink } activeClassName="nav-active" to={ path }>
        { navTitle }
    </Nav.Link>
);

NavItem.propTypes = {
    path: propTypes.node,
    navTitle: propTypes.node,
};
NavItem.defaultProps = {
    path: '',
    navTitle: '',
};

export default NavItem;
