import React from 'react';
import styled from 'styled-components';
import propTypes from 'prop-types';

// Styles
import './Embed.scss';

const Frame = styled.iframe`
    height: 38vh;
    width: 100%;
    border: none;
`;


const Embed = ({ url }) => (
    <Frame title="video tutorial" src={ url } />
);

Embed.propTypes = {
    url: propTypes.node,
};
Embed.defaultProps = {
    url: '',
};

export default Embed;
