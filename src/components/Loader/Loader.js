import React from 'react';
import ContentLoader from 'react-content-loader';

const Loader = () => (
    <ContentLoader
      speed={ 2 }
      width={ 400 }
      height={ 160 }
      viewBox="0 0 400 160"
      backgroundColor="#ebebeb"
      foregroundColor="#c2c2c2"
    >
        <rect x="48" y="8" rx="3" ry="3" width="400" height="160" />
    </ContentLoader>
);

export default Loader;
