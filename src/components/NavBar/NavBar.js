import React, { Component } from 'react';
import styled from 'styled-components';
// import { NavLink } from 'react-router-dom';
import {
    Nav,
    Navbar,
    Container,
} from 'react-bootstrap';

// Component
import NavItem from '../NavItem/NavItem';
import Button from '../Buttons/Buttons';

// Styles
import './NavBar.scss';

// Asstes
import logo from '../../assets/dagoja1.png';

const NavBar = styled(Navbar)`
    border: 1px solid black;
`;

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navItems: [
                {
                    id: 1, path: '/home', title: 'Home', isActive: false,
                },
                {
                    id: 2, path: '/kategori', title: 'Kategori', isActive: false,
                },
                {
                    id: 1, path: '/tentang-kami', title: 'Tentang Kami', isActive: false,
                },
                {
                    id: 1, path: '/kontak-kami', title: 'Kontak Kami', isActive: false,
                },
            ],
        };
    }

    render() {
        const { navItems } = this.state;
        return (
            <NavBar expand="md">
                <Container>
                    <Navbar.Brand href="/home">
                        <img
                          src={ logo }
                          width="50"
                          height="50"
                          className="d-inline-block align-top"
                          alt="React Bootstrap logo"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse
                      id="responsive-navbar-nav"
                      className="justify-content-end"
                    >
                        <Nav>
                            {
                                navItems.map((x) => (
                                    <NavItem
                                      key={ x.id }
                                      path={ x.path }
                                      navTitle={ x.title }
                                    />
                                ))
                            }
                        </Nav>
                        <Nav className="nav-button">
                            {/* <Button
                            link="/register"
                            text="Register"
                            classes="dagoja-btn-register"
                            /> */}
                            <Button text='Register' link='/register' theme='register' />
                        </Nav>
                        <Nav className="nav-button">
                            {/* <Button
                            link="/portal"
                            text="Portal"
                            classes="dagoja-btn-register"
                            /> */}
                            <Button text='Portal' link='/portal' theme='register' />
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </NavBar>
        );
    }
}

export default Navigation;
