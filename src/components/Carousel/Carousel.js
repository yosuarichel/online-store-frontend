import React from 'react';
import {
    Carousel,
} from 'react-bootstrap';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import '../../../node_modules/react-lazy-load-image-component/src/effects/blur.css';

// Styles
import './Carousel.scss';

// Assets
import banner1 from '../../assets/banner1.png';
import banner2 from '../../assets/banner2.png';


const CarouselComp = () => (
    <Carousel>
        <Carousel.Item>
            <LazyLoadImage
              threshold={ 1000 }
              delayTime={ 30 }
              className="w-100"
              alt="First Slide"
              effect="blur"
              src={ banner1 }
            />
            <Carousel.Caption>
                <h3>Build an online business—no matter what business you’re in</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <LazyLoadImage
              className="w-100"
              alt="Second Slide"
              effect="blur"
              src={ banner2 }
            />
            <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
        </Carousel.Item>
    </Carousel>
);

export default CarouselComp;
