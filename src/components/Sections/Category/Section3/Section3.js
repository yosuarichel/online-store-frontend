import React from 'react';
import {
    Row, Col, 
} from 'react-bootstrap';
import styled from 'styled-components';
import { LazyLoadImage } from 'react-lazy-load-image-component';

// Components
import Button from '../../../Buttons/Buttons';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

// Assets
import icon1 from '../../../../assets/icons/phone-ic.png';
import icon2 from '../../../../assets/icons/laptop-ic.png';
import icon3 from '../../../../assets/icons/food-ic.png';
import icon4 from '../../../../assets/icons/guitar-ic.png';
import icon5 from '../../../../assets/icons/supplement-ic.png';
import icon6 from '../../../../assets/icons/dress-ic.png';
import icon7 from '../../../../assets/icons/car-repair-ic.png';
import icon8 from '../../../../assets/icons/motor-repair-ic.png';
import icon9 from '../../../../assets/icons/baby-trolley-ic.png';


const Section = styled.div`
    border: 1px solid black;
    background: #f9f9f9;
    display: -ms-flexbox;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 50px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

// const SectionHeader = styled.div`
//     border: 1px solid black;
//     text-align: center;
//     margin-bottom: 20px;
// `;

// const SectionTitle = styled.h4`
//     border: 1px solid black;
//     font-weight: bold;
//     color: #3f4952;
// `;

// const SectionSubTitle = styled.span`
//     border: 1px solid black;
//     color: #3f4952;
// `;

const SectionContent = styled.div`
    border: 1px solid black;
    align-items: center;
`;

const CardRow = styled(Row)`
    -webkit-transition: width 0.5s ease, margin 0.5s ease;
    -moz-transition: width 0.5s ease, margin 0.5s ease;
    -o-transition: width 0.5s ease, margin 0.5s ease;
    transition: width 0.5s ease, margin 0.5s ease;
    @media (max-width: 768px) {
        margin-top: 40px;
        margin-bottom: 40px;
    }
`;

const CardCol = styled(Col)`
    @media (max-width: 768px) {
        padding-bottom: 30px;
    }
`;

const Card = styled.div`
    /* border: 1px solid black; */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    text-align: center;
    align-items: center;
    height: 100%;
    border-radius: 10px;
    /* background-color: #f1f1f1; */
    padding-top: 10px;
`;

const CardImg = styled.div`
    width: 100%;
    padding: 15px;
`;

const CardBody = styled.div`
    /* border: 1px solid black; */
    padding: 10px 10px 20px 10px;
`;
const CardTitle = styled.h5`
    /* border: 1px solid black; */
    color: #54595f;
    font-weight: bold;
    font-size: 1em;
`;
const CardText = styled.div`
    /* border: 1px solid black; */
    color: #54595f;
`;

const LineBreak = styled.hr`
    margin-top: 70px;
    margin-bottom: 70px;
    border: 1px solid #7a7a7a;
    opacity: 0.5;
    size: 2px;
    @media (max-width: 768px) {
        margin-top: 10px;
        margin-bottom: 10px;
    }
`;


const Section3 = () => (
    <Section>
        <SectionBox>
            {/* <SectionHeader>
                <SectionTitle>Virtual Distributor Interactive Terbaik Berada di Genggaman</SectionTitle>
                <SectionSubTitle>Virtual Distributor Interactive hadir dengan Brand Dagoja Indonesia dengan layanan Customer Care terintgrasi dengan sistem yang dirancang untuk memastikan kemudahan kamu sebagai Mitra Distributor Virtual dari Dagoja Indonesia</SectionSubTitle>
            </SectionHeader> */}
            <SectionContent>
                <CardRow>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 1'
                                  effect='blur'
                                  src={ icon1 }
                                /> 
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Handphone & Aksesoris
                                </CardTitle>
                                <CardText>
                                    Banyak pilihan produk dan tentunya semua produk adalah original, serta tersedia kategori Handphone dan Aksesoris dengan berbagai Chipset dan Spesifikasi terbaik yang bisa di jual.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 2'
                                  effect='blur'
                                  src={ icon2 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Komputer & Aksesoris
                                </CardTitle>
                                <CardText>
                                    Mulai dari Laptop hingga PC Dekstop bisa di jual pada kategori ini, Mulai dari VGA terbaik, Processor, VRAM, serta banyak lagi Hardware dan Software pilihan terbaik bisa di jual.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 3'
                                  effect='blur'
                                  src={ icon3 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Makanan & Minuman
                                </CardTitle>
                                <CardText>
                                    Kategori ini sangat bisa di andalkan karena banyak jenis Makanan & Minuman dengan citarasa terbaik dari Nasional maupun internasional yang bisa dijual dengan harga terbaik.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                </CardRow>
                <CardRow>
                    <CardCol>
                        <LineBreak />
                    </CardCol>
                </CardRow>
                <CardRow>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 1'
                                  effect='blur'
                                  src={ icon4 }
                                /> 
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Hobi & Olahraga
                                </CardTitle>
                                <CardText>
                                    Hobi dan Olahraga adalah kategori yang sejalan, pastinya akan banyak customer yang membutuhkan produk-produk yang mereka butuhkan untuk mendukung Hobi mereka.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 2'
                                  effect='blur'
                                  src={ icon5 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Suplemen Kesehatan
                                </CardTitle>
                                <CardText>
                                    Kesehatan merupakan kebutuhan yang sangat Vital bagi semua individu, sehingga kategori ini cocok bagi kamu yang sangat menyukai Dunia Kesehatan.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 3'
                                  effect='blur'
                                  src={ icon6 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Fashion Pria & Wanita
                                </CardTitle>
                                <CardText>
                                    Kategori yang satu ini sudah tidak asing bagi kita, banyak orang yang sangat menyukai fashion. Sehingga cocok untuk meningkatkan omset.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                </CardRow>
                <CardRow>
                    <CardCol>
                        <LineBreak />
                    </CardCol>
                </CardRow>
                <CardRow>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 1'
                                  effect='blur'
                                  src={ icon7 }
                                /> 
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Aksesoris Mobil
                                </CardTitle>
                                <CardText>
                                    Berbagai produk yang bisa di jual pada kategori ini, Semua produk adalah original dan telah di filter dengan standar kualitas yang bagus. Sehingga kepuasan customer adalah hal utama.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 2'
                                  effect='blur'
                                  src={ icon8 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Aksesoris Motor
                                </CardTitle>
                                <CardText>
                                    Pada kategori ini, Semua produk adalah original dan telah di filter dengan standar kualitas yang bagus. Sehingga kepuasan customer adalah hal utama.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='60px'
                                  width='60px'
                                  alt='icon 3'
                                  effect='blur'
                                  src={ icon9 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Perlengkapan Bayi
                                </CardTitle>
                                <CardText>
                                    Kategori ini adalah Distributor penjual berbagai macam perlengkapan bayi, Mulai dari Stroller, Tempat makan & Minum dan banyak lagi.
                                </CardText>
                                <Button text='Contoh Produk' link='/example' theme='card' />
                            </CardBody>
                        </Card>
                    </CardCol>
                </CardRow>
            </SectionContent>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section3;
