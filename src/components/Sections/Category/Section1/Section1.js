import React from 'react';
import {
    Row, Col, Image
} from 'react-bootstrap';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Styles
import './Section1.scss';

// Component

// Assets
import bgImage from '../../../../assets/bg/back-back.png';
import Illustration1 from '../../../../assets/illustration/img2.png';

const Section = styled.div`
    border: 1px solid black;
    background: url(${ bgImage });
    /* opacity: 1; */
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    display: -ms-flexbox;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 20px;
    /* ::before {
        opacity: 0.5;
    } */
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    margin-bottom: 20px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #54595f;
    font-size: 2.3em;
`;

// const SectionSubTitle = styled.span`
//     border: 1px solid black;
//     color: #fff;
//     font-size: 1.5em;
// `;

const SectionContent = styled.div`
    border: 1px solid black;
    align-items: center;
    color: #7a7a7a;
`;

const ContentList = styled.div`
    margin-bottom: 30px;
`;

const SectionRow = styled(Row)`
    -webkit-transition: width 0.5s ease, margin 0.5s ease;
    -moz-transition: width 0.5s ease, margin 0.5s ease;
    -o-transition: width 0.5s ease, margin 0.5s ease;
    transition: width 0.5s ease, margin 0.5s ease;
`;

const SectionCol = styled(Col)`
    border: 1px solid black;
    @media (max-width: 768px) {
        text-align: left;
    }
`;

// const Icon = styled(FontAwesomeIcon)`
//     margin-right: 5px;
// `;

const Section1 = () => (
    <Section>
        <SectionBox>
            <SectionRow>
                <SectionCol md>
                    <SectionHeader>
                        <SectionTitle>Pilih Kategori Distributor</SectionTitle>
                    </SectionHeader>
                    <SectionContent>
                        <p>
                            Fokuskan kepada promosi Virtual Distributor kamu dan perbanyak Reseller agar target tercapai tanpa harus pusing memikirkan sistem. Kamu mendapat semua dukungan dari DAGOJA.
                        </p>
                        <ContentList>
                            <SectionRow>
                                <SectionCol md>
                                    <ul className="icon-lists">
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="check-circle" size="1x" className="icon-list" />
                                            Web Seminar.
                                        </li>
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="check-circle" size="1x" className="icon-list" />
                                            Portal Mitra.
                                        </li>
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="check-circle" size="1x" className="icon-list" />
                                            Dukungan dari segala sisi.
                                        </li>
                                    </ul>
                                </SectionCol>
                                <SectionCol md>
                                    <ul className="icon-lists">
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="times-circle" size="1x" className="icon-list" />
                                            Biaya Bulanan.
                                        </li>
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="times-circle" size="1x" className="icon-list" />
                                            Biaya Konten Promosi.
                                        </li>
                                        <li className="icon-list-item">
                                            <FontAwesomeIcon icon="times-circle" size="1x" className="icon-list" />
                                            Biaya Mentoring.
                                        </li>
                                    </ul>
                                </SectionCol>
                            </SectionRow>
                        </ContentList>
                    </SectionContent>
                </SectionCol>
                <SectionCol md className='text-center'>
                    <SectionContent>
                        <Image src={ Illustration1 } style={ {height:'auto',width:'80%'} } />
                    </SectionContent>
                </SectionCol>
            </SectionRow>
        </SectionBox>
    </Section>
);

// Section1.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };

export default Section1;
