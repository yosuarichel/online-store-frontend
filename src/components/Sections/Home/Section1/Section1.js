import React, { Suspense } from 'react';
import {
    Row, Col,
} from 'react-bootstrap';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Loader from '../../../Loader/Loader';

// Styles
import './Section1.scss';
// import { LazyLoadComponent } from 'react-lazy-load-image-component';

// Component
import Button from '../../../Buttons/Buttons';

const Embed = React.lazy(() => import('../../../Embed/Embed'));

const Section = styled.div`
    border: 1px solid black;
    background: #3F4952;
    display: -ms-flexbox;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 100px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    margin-bottom: 20px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #fff;
    font-size: 2.3em;
`;

const SectionSubTitle = styled.span`
    border: 1px solid black;
    color: #fff;
    font-size: 1.5em;
`;

const SectionContent = styled.div`
    border: 1px solid black;
    align-items: center;
    color: #fff;
`;

const ContentList = styled.div`
    margin-bottom: 30px;
`;

const SectionCol = styled(Col)`
    border: 1px solid black;
    @media (max-width: 768px) {
        text-align: left;
    }
`;

const EmbedVideo = styled(Embed)`
    margin-top: 10px;
`;

// const Icon = styled(FontAwesomeIcon)`
//     margin-right: 5px;
// `;

const Section1 = () => (
    // <Wrapper>
    //     <Container>
    //         <Row>
    //             <Col md>
    //                 <div className="content-section1">
    //                     <h1>Virtual Distributor Interactive</h1>
    //                     <h4>By DAGOJA</h4>
    //                     <ul className="icon-lists">
    //                         <li className="icon-list-item">
    //                             <FontAwesomeIcon icon="check-circle" size="1x" className="icon-list" />
    //                             Kontent Promosi
    //                         </li>
    //                         <li className="icon-list-item">
    //                             <FontAwesomeIcon icon="briefcase" size="1x" className="icon-list" />
    //                             Produk Disediakan
    //                         </li>
    //                         <li className="icon-list-item">
    //                             <FontAwesomeIcon icon="credit-card" size="1x" className="icon-list" />
    //                             Keamanan Pembayaran
    //                         </li>
    //                     </ul>
    //                     <br />
    //                     <p>
    //                         Kini jadi distributor tidak lagi membutuhkan biaya yang besar untuk membangun bisnis dan sistem yang kuat.
    //                     </p>
    //                 </div>
    //             </Col>
    //             <Col md className="text-center">
    //                 <Row>
    //                     <Col>
    //                         <Suspense fallback={ Loader }>
    //                             <Embed />
    //                         </Suspense>
    //                     </Col>
    //                 </Row>
    //                 <Row>
    //                     <Col md className="button-place">
    //                         {/* <div className="button-item">
    //                             <Buttons text="Cek Kategori Produk" icon="list-alt" classes="dagoja-btn" left />
    //                         </div> */}
    //                         <Buttons text="Cek Kategori Produk" icon="list-alt" classes="dagoja-btn" left />
    //                     </Col>
    //                     <Col md className="button-place">
    //                         {/* <div className="button-item">
    //                             <Buttons text="Gabung Sekarang" icon="list-alt" classes="dagoja-btn-yellow" left />
    //                         </div> */}
    //                         <Buttons text="Gabung Sekarang" icon="list-alt" classes="dagoja-btn-yellow" left />
    //                     </Col>
    //                 </Row>
    //             </Col>
    //         </Row>
    //     </Container>
    // </Wrapper>
    <Section>
        <SectionBox>
            <Row>
                <SectionCol md>
                    <SectionHeader>
                        <SectionTitle>Virtual Distributor Interactive</SectionTitle>
                        <SectionSubTitle>By DAGOJA</SectionSubTitle>
                    </SectionHeader>
                    <SectionContent>
                        <ContentList>
                            <ul className="icon-lists">
                                <li className="icon-list-item">
                                    <FontAwesomeIcon icon="check-circle" size="1x" className="icon-list" />
                                    Kontent Promosi
                                </li>
                                <li className="icon-list-item">
                                    <FontAwesomeIcon icon="briefcase" size="1x" className="icon-list" />
                                    Produk Disediakan
                                </li>
                                <li className="icon-list-item">
                                    <FontAwesomeIcon icon="credit-card" size="1x" className="icon-list" />
                                    Keamanan Pembayaran
                                </li>
                            </ul>
                        </ContentList>
                        <p>
                            Kini jadi distributor tidak lagi membutuhkan biaya yang besar untuk membangun bisnis dan sistem yang kuat.
                        </p>
                    </SectionContent>
                </SectionCol>
                <SectionCol md className='text-center'>
                    <SectionContent>
                        <Suspense fallback={ Loader }>
                            <EmbedVideo url='https://www.youtube.com/embed/7sDY4m8KNLc' />
                        </Suspense>
                        <Row className="justify-content-md-center">
                            <SectionCol md={ 5 } className="text-center">
                                <Button text='Cek Kategori Produk' icon='list-alt' link='/text' theme='default' />
                            </SectionCol>
                            <SectionCol md={ 5 } className="text-center">
                                <Button text='Gabung Sekarang' icon='list-alt' link='/text' theme='yellow' />
                            </SectionCol>
                        </Row>
                    </SectionContent>
                </SectionCol>
            </Row>
        </SectionBox>
    </Section>
);

// Section1.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };

export default Section1;
