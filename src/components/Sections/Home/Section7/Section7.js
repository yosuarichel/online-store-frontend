import React from 'react';
import styled from 'styled-components';
import {
    Row, Col, 
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

const Section = styled.div`
    border: 1px solid black;
    background: #3F4952;
    display: -ms-flexbox;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 10px;
    padding-bottom: 10px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionCol = styled(Col)`
    border: 1px solid black;
    @media (max-width: 768px) {
        text-align: center;
    }
`;

const SectionColSpan = styled.div`
    line-height: 50px;
    border: 1px solid black;
    font-size: 1.5em;
    color: #ffffff;
`;

const Button = styled.a`
    display: inline-block;
    padding: 0.6em 1.5em;
    border: 0.2em solid #ffffff;
    border-radius: 1.5em;
    text-decoration: none;
    font-family: 'Roboto', sans-serif;
    font-weight: 300;
    color: #ffffff;
    text-align: center;
    transition: all 0.2s;
    &:hover {
        text-decoration: none;
        color:#000000;
        background-color:#FFFFFF;
    }
`;

const Icon = styled(FontAwesomeIcon)`
    margin-right: 5px;
`;


const Section7 = () => (
    <Section>
        <SectionBox>
            <Row>
                <SectionCol md={ 8 }>
                    <SectionColSpan>Perlu BANTUAN? Hubungi Kami : 021-8238-9990</SectionColSpan>
                </SectionCol>
                <SectionCol md className='text-center'>
                    <Button href='text'>
                        <Icon icon="headset" size="1x" />
                        Customer Care
                    </Button>
                </SectionCol>
            </Row>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section7;
