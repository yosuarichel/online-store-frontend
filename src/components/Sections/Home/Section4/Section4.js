import React from 'react';
import {
    Row, Col, 
} from 'react-bootstrap';
import styled from 'styled-components';
import { LazyLoadImage } from 'react-lazy-load-image-component';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

// Assets
import icon1 from '../../../../assets/icons/truck.png';
import icon2 from '../../../../assets/icons/book.png';
import icon3 from '../../../../assets/icons/phone.png';

const Section = styled.div`
    border: 1px solid black;
    background: #ffffff;
    display: -ms-flexbox;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 50px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    text-align: center;
    margin-bottom: 20px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #3f4952;
`;

const SectionSubTitle = styled.span`
    border: 1px solid black;
    color: #3f4952;
`;

const SectionContent = styled.div`
    border: 1px solid black;
    align-items: center;
`;

const CardRow = styled(Row)`
`;

const CardCol = styled(Col)`
    @media (max-width: 768px) {
        padding-bottom: 15px;
  }
`;

const Card = styled.div`
    border: 1px solid black;
    /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); */
    text-align: center;
    align-items: center;
    /* background-color: #f1f1f1; */
    /* padding: 0 10px; */
`;

const CardImg = styled.div`
    width: 100%;
`;

const CardBody = styled.div`
    border: 1px solid black;
    padding: 5px;
`;
const CardTitle = styled.h5`
    border: 1px solid black;
    font-weight: bold;
    font-size: 1em;
`;
const CardText = styled.div`
    border: 1px solid black;
    color: #3f4952;
`;


const Section4 = () => (
    <Section>
        <SectionBox>
            <SectionHeader>
                <SectionTitle>Virtual Distributor Interactive Terbaik Berada di Genggaman</SectionTitle>
                <SectionSubTitle>Virtual Distributor Interactive hadir dengan Brand Dagoja Indonesia dengan layanan Customer Care terintgrasi dengan sistem yang dirancang untuk memastikan kemudahan kamu sebagai Mitra Distributor Virtual dari Dagoja Indonesia</SectionSubTitle>
            </SectionHeader>
            <SectionContent>
                <CardRow>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='100px'
                                  width='100px'
                                  alt='icon 1'
                                  effect='blur'
                                  src={ icon1 }
                                /> 
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Stok Barang/Produk Disediakan
                                </CardTitle>
                                <CardText>
                                    DAGOJA menyediakan produk yang bisa kamu jual. Pastinya barang/produk yang disediakan adalah Produk Branded dan Original.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='100px'
                                  width='100px'
                                  alt='icon 2'
                                  effect='blur'
                                  src={ icon2 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Full Support Konten Jualan
                                </CardTitle>
                                <CardText>
                                    Kamu akan mendapatkan berbagai konten produk untuk di share ke akun social media kamu dan Platform E-commerce yang ada.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 1000 }
                                  delayTime={ 30 }
                                  height='100px'
                                  width='100px'
                                  alt='icon 3'
                                  effect='blur'
                                  src={ icon3 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Customer/Reseller Handling
                                </CardTitle>
                                <CardText>
                                    Reseller kamu? Dagoja yang atur, Customer Care? Dagoja yang handle, Pengiriman? Dagoja yang handle, kamu hanya belajar dan upload-upload konten saja.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                </CardRow>
            </SectionContent>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section4;
