import React from 'react';
import {
    Row, Col, Image
} from 'react-bootstrap';
import styled from 'styled-components';

// Styles
import './Section3.scss';
import 'react-lazy-load-image-component/src/effects/blur.css';

// Assets
import Img1 from '../../../../assets/illustration/img1.png';


const Section = styled.div`
    border: 1px solid black;
    background: #F2F8F4;
    display: -ms-flexbox;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 50px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    margin-bottom: 20px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #3F4952;
    font-size: 2.3em;
`;

// const SectionSubTitle = styled.span`
//     border: 1px solid black;
//     color: #fff;
//     font-size: 1.5em;
// `;

// const SectionContent = styled.div`
//     border: 1px solid black;
//     align-items: center;
//     color: #fff;
// `;

const SectionCol = styled(Col)`
    border: 1px solid black;
    text-align: left;
`;


const Section2 = () => (
    // <Row className="div-section3">
    //     <Container>
    //         <Row className="align-items-center">
    //             <Col md>
    //                 <div className="content-section3">
    //                     <h2>Jadi Distributor Tanpa Modal Besar</h2>
    //                     <p>
    //                         Kini jadi distributor tidak lagi membutuhkan biaya yang besar untuk membangun bisnis dan sistem yang kuat. Tidak perlu khawatir untuk membangun image yang baik untuk pelanggan kamu, karena di Dagoja kami sediakan sistem, konten dan pastinya akan ada Webinar untuk membantu kamu mengembangkan Brand Distributor kamu.
    //                     </p>
    //                 </div>
    //             </Col>
    //             <Col md className="text-center">
    //                 {/* <ResponsiveEmbed aspectRatio="1by1">
                        
    //                 </ResponsiveEmbed> */}
    //                 <Image src={ Img1 } style={ {height:'auto',width:'80%'} } />
    //             </Col>
    //         </Row>
    //     </Container>
    // </Row>
    <Section>
        <SectionBox>
            <Row>
                <SectionCol md>
                    <SectionHeader>
                        <SectionTitle>Jadi Distributor Tanpa Modal Besar</SectionTitle>
                        <p>Kini jadi distributor tidak lagi membutuhkan biaya yang besar untuk membangun bisnis dan sistem yang kuat. Tidak perlu khawatir untuk membangun image yang baik untuk pelanggan kamu, karena di Dagoja kami sediakan sistem, konten dan pastinya akan ada Webinar untuk membantu kamu mengembangkan Brand Distributor kamu.</p>
                    </SectionHeader>
                </SectionCol>
                <SectionCol md className='text-center'>
                    <Image src={ Img1 } style={ {height:'auto',width:'80%'} } />
                </SectionCol>
            </Row>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };

export default Section2;
