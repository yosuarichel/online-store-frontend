import React from 'react';
import styled from 'styled-components';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

const Section = styled.div`
    border: 1px solid black;
    background: #ffffff;
    display: -ms-flexbox;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 50px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    text-align: center;
    padding-bottom: 5px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #686868;
`;

const SectionSubTitle = styled.span`
    border: 1px solid black;
    color: #7a7a7a;
    font-size: 1.2em;
`;

const SectionContent = styled.div`
    border: 1px solid black;
    color: #7a7a7a;
    font-weight: bold;
    text-align: center;
    font-size: 1.2em;
`;


const Section6 = () => (
    <Section>
        <SectionBox>
            <SectionHeader>
                <SectionTitle>Perbandingan Penghematan Modal Awal untuk Distributor Konvensional Jika Bergabung Menjadi Virtual Distributor DAGOJA</SectionTitle>
                <SectionSubTitle>Kamu bisa hemat Biaya operasional</SectionSubTitle>
            </SectionHeader>
            <SectionContent>
                RP 198.475.000/bulan
            </SectionContent>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section6;
