import React from 'react';
import {
    Row, Col, 
} from 'react-bootstrap';
import styled from 'styled-components';
import { LazyLoadImage } from 'react-lazy-load-image-component';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

// Assets
import icon1 from '../../../../assets/icons/account.png';
import icon2 from '../../../../assets/icons/check.png';
import icon3 from '../../../../assets/icons/safe.png';

const Section = styled.div`
    border: 1px solid black;
    background: #f2f8f4;
    display: -ms-flexbox;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 50px;
    padding-bottom: 50px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionHeader = styled.div`
    border: 1px solid black;
    text-align: center;
    margin-bottom: 20px;
`;

const SectionTitle = styled.h4`
    border: 1px solid black;
    font-weight: bold;
    color: #3f4952;
`;

const SectionSubTitle = styled.span`
    border: 1px solid black;
    color: #3f4952;
`;

const SectionContent = styled.div`
    border: 1px solid black;
    align-items: center;
`;

const CardCol = styled(Col)`
    @media (max-width: 768px) {
        padding-bottom: 15px;
    }
`;

const Card = styled.div`
    border: 1px solid black;
    text-align: center;
    align-items: center;
`;

const CardImg = styled.div`
    width: 100%;
`;

const CardBody = styled.div`
    border: 1px solid black;
    padding: 5px;
`;
const CardTitle = styled.h5`
    border: 1px solid black;
    font-weight: bold;
    font-size: 1em;
`;
const CardText = styled.div`
    border: 1px solid black;
    color: #3f4952;
`;


const Section5 = () => (
    <Section>
        <SectionBox>
            <SectionHeader>
                <SectionTitle>Fasilitas Keamanan Pembayaran Pesanan</SectionTitle>
                <SectionSubTitle>Virtual Distributor Interactive hadir dengan Brand Dagoja Indonesia dengan layanan Customer Care terintgrasi dengan sistem yang dirancang untuk memastikan kemudahan kamu sebagai Mitra Distributor Virtual dari Dagoja Indonesia.</SectionSubTitle>
            </SectionHeader>
            <SectionContent>
                <Row>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 10000 }
                                  delayTime={ 100 }
                                  height='90px'
                                  width='110px'
                                  alt='icon 1'
                                  effect='blur'
                                  src={ icon1 }
                                /> 
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Distributor ID Protection
                                </CardTitle>
                                <CardText>
                                    Dagoja memastikan setiap data Mitra Distributor terlindungi dengan baik dengan sistem otoritas akses yang sangat terbatas.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 10000 }
                                  delayTime={ 100 }
                                  height='90px'
                                  width='110px'
                                  alt='icon 2'
                                  effect='blur'
                                  src={ icon2 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Online Store Payment Secure
                                </CardTitle>
                                <CardText>
                                    Sistem keamanan yang ada pada Online Store terintegrasi dengan Mitra Payment Gateway yang di pilih oleh DAGOJA.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                    <CardCol md>
                        <Card>
                            <CardImg>
                                <LazyLoadImage
                                  threshold={ 10000 }
                                  delayTime={ 100 }
                                  height='90px'
                                  width='110px'
                                  alt='icon 3'
                                  effect='blur'
                                  src={ icon3 }
                                />
                            </CardImg>
                            <CardBody>
                                <CardTitle>
                                    Reseller Order Protection
                                </CardTitle>
                                <CardText>
                                    Reseller yang adalah Customer kamu akan mendapatkan fasilitas penuh dalam layanan Customer Care dan Customer Handle khusus reseller kamu.
                                </CardText>
                            </CardBody>
                        </Card>
                    </CardCol>
                </Row>
            </SectionContent>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section5;
