import React from 'react';
import {
    Row, Col, Container,
} from 'react-bootstrap';

// Styles
import './Section2.scss';


const Section2 = () => (
    <Row className="">
        <Container>
            <Row className="section2 text-center">
                <Col md>
                    <div className="content-section2">
                        <h5>
                            5
                        </h5>
                        <span>LESSONS</span>
                    </div>
                </Col>
                <Col md>
                    <div className="content-section2">
                        <h5>
                            4
                        </h5>
                        <span>VIDEOS</span>
                    </div>
                </Col>
                <Col md>
                    <div className="content-section2">
                        <h5>
                            ALL
                        </h5>
                        <span>SKILL LEVELS</span>
                    </div>
                </Col>
                <Col md>
                    <div className="content-section2">
                        <h5>
                            4 H
                        </h5>
                        <span>DURATION</span>
                    </div>
                </Col>
                <Col md>
                    <div className="content-section2">
                        <h5>
                            English
                        </h5>
                        <span>LANGUAGE</span>
                    </div>
                </Col>
            </Row>
        </Container>
    </Row>
);

export default Section2;