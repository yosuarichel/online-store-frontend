import React from 'react';
import styled from 'styled-components';

// Styles
import 'react-lazy-load-image-component/src/effects/blur.css';

// Assets
import image1 from '../../../../assets/icons/payment-gateway.png';

const Section = styled.div`
    border: 1px solid black;
    background: #ffffff;
    display: -ms-flexbox;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
    padding-top: 10px;
    padding-bottom: 10px;
`;

const SectionBox = styled.div`
    border: 1px solid black;
    max-width: 1240px;
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
`;

const SectionContentImage = styled.div`
    border: 1px solid black;
    text-align: center;
    padding-bottom: 5px;
    img {
        width: 600px;
        height: 50px;
        @media (max-width: 768px) {
            width: 400px;
            height: 30px;
        }
        @media (max-width: 484px) {
            width: 300px;
            height: 25px;
        }
    }
`;

const SectionContent = styled.div`
    border: 1px solid black;
    color: #333333;
    font-weight: bold;
    text-align: center;
    font-size: 0.8em;
`;


const Section8 = () => (
    <Section>
        <SectionBox>
            <SectionContent>
                <SectionContentImage>
                    <img src={ image1 } width='600px' height='50px' alt='payment gateway' />
                </SectionContentImage>
                Copyright 2020 PT. Toming Kailash Curtina. All Rights Reserved.
            </SectionContent>
        </SectionBox>
    </Section>
);

// Section2.propTypes = {
//     path: propTypes.node,
//     navTitle: propTypes.node,
//     class: propTypes.node,
// };
export default Section8;
