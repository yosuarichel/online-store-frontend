// import the library
import { library } from '@fortawesome/fontawesome-svg-core';

// import your icons
import {
    faCode,
    faHighlighter,
    faCheckCircle,
    faCreditCard,
    faBriefcase,
    faListAlt,
    faHeadset,
    faTimesCircle,
    faMobileAlt,
} from '@fortawesome/free-solid-svg-icons';

library.add(
    faCode,
    faHighlighter,
    faCheckCircle,
    faCreditCard,
    faBriefcase,
    faListAlt,
    faHeadset,
    faTimesCircle,
    faMobileAlt
);
