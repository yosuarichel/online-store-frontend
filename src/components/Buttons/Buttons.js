/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Link } from 'react-router-dom';
// import {
//     Button,
// } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import propTypes from 'prop-types';

// Styles
import './Buttons.scss';

const handleButtontheme = theme => {
    switch (theme) {
    case 'yellow':
        return {
            bgColor: '#FDDC5B',
            bgColorHover: '#006BDA',
            color: '#000',
            fontFamily: '"Roboto", sans-serif',
            padding: '7px 20px',
            margin: '10px 0',
            border: 'none',
            borderRadius: '1.5em',
            borderHover: 'none',
        };
    case 'register':
        return {
            bgColor: 'transparent',
            bgColorHover: '#7A7A7A',
            color: '#000',
            fontFamily: '',
            padding: '1px 20px',
            margin: '7px 0',
            border: 'solid 1px #000',
            borderRadius: '50px',
            borderHover: 'solid 1px #7A7A7A',
        };
    case 'card':
        return {
            bgColor: 'transparent',
            bgColorHover: '#7A7A7A',
            color: '#54595f',
            fontFamily: '',
            padding: '1px 20px',
            margin: '7px 0',
            border: 'solid 1px #000',
            borderRadius: '5px',
            borderHover: 'solid 1px #7A7A7A',
        };
    default:
        return {
            bgColor: '#fff',
            bgColorHover: '#7A7A7A',
            color: '#000',
            fontFamily: '"Roboto", sans-serif',
            padding: '7px 20px',
            margin: '10px 0',
            border: 'none',
            borderRadius: '1.5em',
            borderHover: 'none',
        };
    }
};

const Button = styled.button`
    display: inline-block;
    padding: ${({ theme }) => handleButtontheme(theme).padding};
    border: ${({ theme }) => handleButtontheme(theme).border};
    background: ${({ theme }) => handleButtontheme(theme).bgColor};
    border-radius: ${({ theme }) => handleButtontheme(theme).borderRadius};
    text-decoration: none;
    font-family: ${({ theme }) => handleButtontheme(theme).fontFamily};
    font-weight: 300;
    color: ${({ theme }) => handleButtontheme(theme).color};
    text-align: center;
    transition: all 0.2s;
    margin: ${({ theme }) => handleButtontheme(theme).margin};
    &:hover {
        text-decoration: none;
        color:#fff;
        background-color: ${({ theme }) => handleButtontheme(theme).bgColorHover};
        border: ${({ theme }) => handleButtontheme(theme).borderHover};
        border-radius: ${({ theme }) => handleButtontheme(theme).borderRadius};
    }
    &:focus {
        border: none;
        outline: none;
        text-decoration: none;
        color:#fff;
        background-color: ${({ theme }) => handleButtontheme(theme).bgColorHover};
        border: ${({ theme }) => handleButtontheme(theme).borderHover};
        border-radius: ${({ theme }) => handleButtontheme(theme).borderRadius};
    }
    @media (max-width: 768px) {
        margin-top: 10px;
        margin-bottom: 10px;
    }
`;

const Icon = styled(FontAwesomeIcon)`
    margin-right: 8px;
`;

const Buttons = ({ text, link, icon, theme }) => (
    <Link to={ link }>
        <Button theme={ theme }>
            <Icon icon={ icon } size="1x" />
            { text }
        </Button>
    </Link>
);

Buttons.propTypes = {
    text: propTypes.node,
    theme: propTypes.node,
    icon: propTypes.node,
    link: propTypes.node,
};
Buttons.defaultProps = {
    text: 'Default',
    theme: 'default',
    icon: '',
    link: '',
};

export default Buttons;
