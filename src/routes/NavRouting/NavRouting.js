import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// Component Pages
import Home from '../../containers/Pages/Home/Home';
import Category from '../../containers/Pages/Category/Category';
// import Portal from '../Pages/Portal/Portal';

const Routing = () => (
    <Switch>
        <Route exact path="/" component={ Home }>
            <Redirect to="/home" />
        </Route>
        <Route path="/home" component={ Home } />
        <Route path="/kategori" component={ Category } />
        <Route path="/tentang-kami">
            <h2>Tentang</h2>
        </Route>
        <Route path="/kontak-kami">
            <h2>Kontak Kami</h2>
        </Route>
        <Route path="/portal">
            <h2>Portal</h2>
        </Route>
    </Switch>
);

export default Routing;
