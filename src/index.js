import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Helmet } from 'react-helmet';
// import { Provider } from 'react-redux';
// import * as Sentry from '@sentry/browser';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './sass/main.scss';

import App from './containers/App/App';
import * as serviceWorker from './serviceWorker';

// const routes = createRoutes();

// Sentry.init({
//     dsn: process.env.REACT_APP_SENTRY_DSN,
//     environment: process.env.REACT_APP_SENTRY_ENV,
// });

ReactDOM.render(
    <Router>
        <Helmet>
            <title>React Boilerplate</title>
            <meta
              name="description"
              content="a simple react boilerplate with webpack and babel"
            />
            {/* <link rel="canonical" href="https://movies.fidalgo.dev" /> */}
        </Helmet>
        <App />
    </Router>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
